const moment = require("moment");
module.exports = (req, res, next) => {
  var aux = 0;
  req.body.intervals.map(interval => {
    var { start, end } = interval;
    start = moment(start.replace(":", ""), "hmm").format("HH:mm");
    end = moment(end.replace(":", ""), "hmm").format("HH:mm");
    if (start == "Invalid date" || end == "Invalid date") aux++;
  });
  if (aux) return res.json("invalid intervals");
  return next();
};
