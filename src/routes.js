const routes = require("express").Router();
const RuleController = require("./controllers/RuleController");
const RoleMiddleware = require("./middlewares/RuleMiddleware");
routes.get("/", RuleController.index);

routes.get("/rules", RuleController.index);

routes.post("/rules", RoleMiddleware, RuleController.store);

routes.delete("/rules", RuleController.destroy);
module.exports = routes;
