const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const RuleModel = require("../models/RuleModel");
const adapter = new FileSync("rules.json");

const db = low(adapter);

module.exports = {
  async index(req, res) {
    const { from, to } = req.query;
    //retornar horarios num intervalo de dias
    if (req.query.hasOwnProperty("from") && req.query.hasOwnProperty("to"))
      return res.send(await RuleModel.getByInterval(req.query));

    //retornar horarios num dia especifico

    if (req.query.hasOwnProperty("day"))
      return res.send(await RuleModel.find(req.query));

    return res.send(await RuleModel.get());
  },

  async store(req, res) {
    const rules = await RuleModel.create(req.body);
    return res.json(rules);
  },

  async destroy(req, res) {
    const rules = await RuleModel.delete(req.body);
    return res.json(rules);
  }
};
