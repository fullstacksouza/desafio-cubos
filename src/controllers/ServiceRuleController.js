const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("rules.json");
const db = low(adapter);

module.exports = {
  index(req, res) {
    return res.json({});
  },

  async store(req, res) {
    const rules = await db
      .get("rules")
      .find({ id: req.body.rule_id })
      .value();
    if (!rules) return res.send("regra não cadastrada");
  },

  async destroy(req, res) {
    await db
      .get("rules")
      .remove({ id: req.body.id })
      .write();
    const rules = await db.get("rules").value();
    return res.json(rules);
  }
};
