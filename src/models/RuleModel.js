const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("rules.json");
const db = low(adapter);
const shortid = require("shortid");
const moment = require("moment");

module.exports = {
  async create(newRule) {
    var rules = await db.get("rules").value();
    if (!rules) await db.defaults({ rules: [] }).write();
    const existRule = await this.get({ day: newRule.day });
    console.log(existRule, "exist");
    //verificando se a regra ja foi cadastrada
    if (existRule) {
      db.get("rules")
        .find({ day: existRule.day })
        .push(newRule.intervals)
        .write();
      rules = await db.get("rules").value();
      return rules;
      return "aleady exist rules to this day";
    }
    db.get("rules")
      .push(newRule)
      .write();
    rules = await db.get("rules").value();
    return rules;
  },
  async get(params, filters) {
    const rules = params
      ? await db
          .get("rules")
          .find(params)
          .value()
      : db.get("rules").value();
    return rules;
  },
  async getByInterval(intervals) {
    console.log("by interval");
    var rules = await db
      .get("rules")
      .filter(function(rules) {
        var fromDate = moment(intervals.from, "DD-MM-YYYY");
        var toDate = moment(intervals.to, "DD-MM-YYYY");
        var currentDate = moment(rules.day, "DD-MM-YYYY");
        currentDate.isSameOrAfter(fromDate)
          ? console.log(currentDate.format("DD-MM-YYYY"))
          : null;
        return (
          currentDate.isSameOrAfter(fromDate) &&
          currentDate.isSameOrBefore(toDate)
        );
      })
      .value();
    return rules;
  },
  async find(day) {
    const rules = await db
      .get("rules")
      .find(day)
      .value();
    return rules;
  },
  async delete(id) {
    const existRule = await this.find(id);
    if (!existRule || existRule == undefined) return "rule does not exist";
    const deleteRule = await db
      .get("rules")
      .remove(id)
      .write();
    const rules = await this.get();
    return rules;
  },
  async update(day, values) {
    const rule = await db
      .get("rules")
      .find({ day: day })
      .push({ intervals: 1 }) // or .defaults depending on what you want to do
      .write();

    return rule;
  }
};
