# Desafio Cubos backend

`git clone git@gitlab.com:iscuiz/desafio-cubos.git`

## Iniciar o servidor

`yarn start` ou `npm start`

## Postman Collections

<https://www.getpostman.com/collections/04043a58d36d65cb4892>

## Exemplos de requisição
